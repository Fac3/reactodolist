export default {
  root: {
    flexGrow: 1
  },
  taskList: {
    width: 500
  },
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    paddingTop: 15
  },
  cardsToDo: {
    paddingTop: 5,
    paddingBottom: 5
  },
  actionsToDo: {
    display: "flex"
  }
};
