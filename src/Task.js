import React, { Component } from "react";

import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import { CardContent } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Icon from "@material-ui/core/Icon";
import TextField from "@material-ui/core/TextField";

export default class Task extends Component {
  renderTask() {
    const { task } = this.props;

    if (this.props.task.isBeingEdited) {
      return (
        <TextField
          variant="outlined"
          fullWidth
          label="Nom de la tâche"
          margin="normal"
          defaultValue={this.props.task.name}
          onChange={this.props.handleTaskEditing}
        />
      );
    }

    if (!this.props.task.isBeingEdited) {
      return <Typography variant="h5">{task.name}</Typography>;
    }
  }

  renderActionButtons() {
    const { id } = this.props;
    if (!this.props.task.isBeingEdited) {
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <IconButton
            onClick={() => this.props.handleEdit(this.props.task.id)}
            style={{ color: "#FFC104" }}
          >
            <Icon>edit</Icon>
          </IconButton>
          <IconButton
            onClick={() => this.props.deleteTask(id)}
            style={{ color: "red" }}
          >
            <Icon>delete</Icon>
          </IconButton>
        </div>
      );
    }

    if (this.props.task.isBeingEdited) {
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center"
          }}
        >
          <IconButton
            onClick={() => this.props.saveEditedTask(this.props.task.id)}
            style={{ color: "#FFC104" }}
          >
            <Icon>save</Icon>
          </IconButton>
          <IconButton onClick={() => this.props.handleEditCancel(this.props.task.id)} style={{ color: "red" }}>
            <Icon>cancel</Icon>
          </IconButton>
        </div>
      );
    }
  }

  render() {
    return (
      <Grid item xs={12} md={12} lg={12}>
        <div className="taskCard">
        <Card>
          <CardContent>
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center"
              }}
            >
              {this.renderTask()}
              {this.renderActionButtons()}
            </div>
          </CardContent>
        </Card>
        </div>
      </Grid>
    );
  }
}
