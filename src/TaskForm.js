import React, { Component } from "react";
import styles from "./Styles";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Icon from "@material-ui/core/Icon";
import Fab from "@material-ui/core/Fab";

export default class TaskForm extends Component {
  render() {
    return (
      <Grid item xs={12} md={9} lg={6}>
        <form
          className={styles.formContainer}
          onSubmit={this.props.handleSubmit}
        >
          <Grid
            container
            direction="row"
            alignItems="center"
            justify="space-between"
            spacing={16}
          >
            <Grid item lg={10}>
              <TextField
                variant="outlined"
                fullWidth
                label="Nom de la tâche"
                margin="normal"
                value={this.props.taskName}
                onChange={this.props.handleTaskInput}
              />
            </Grid>
            <Grid item lg={2}>
              <Fab color="primary" aria-label="Add" type="submit">
                <Icon>add</Icon>
              </Fab>
            </Grid>
          </Grid>
        </form>
      </Grid>
    );
  }
}
