import { createMuiTheme } from "@material-ui/core/styles";
import indigo from "@material-ui/core/colors/indigo";

export default createMuiTheme({
  palette: {
    primary: {
      main: "#2ECC40"
    },
    secondary: indigo
  },
  papers: {
    paddingTop: 16,
    paddingBottom: 16
  }
});
