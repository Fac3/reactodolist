import React, { Component } from "react";
import Task from "./Task";

export default class TaskContainer extends Component {
  render() {
    const { taskList } = this.props;
    return taskList.map((task, key) => {
      return (
        <Task
          key={key}
          id={task.id}
          task={task}
          deleteTask={this.props.deleteTask}
          isEditing={this.props.isEditing}
          handleEdit={this.props.handleEdit}
          handleTaskEditing={this.props.handleTaskEditing}
          saveEditedTask={this.props.saveEditedTask}
          handleEditCancel={this.props.handleEditCancel}
        />
      );
    });
  }
}
